#pragma once
#include <stdint.h>
#include <exception>
#include <string>

/**
* ����� Stack ��� �������� ������ ��������� ����.
* ��� ��� �� ���� ������ ���������� �� �����������, ���������� ��� LIFO.
* ��� ��� �� ���� ������ ���������� �� ������������������ � �������� ������������: 
*    � ��������� �����������: 
*		�������: ��� �����;
*		���������: � ����� ����, ��������������� �� ������ ������ �� ���������� ����.
* ��� ��� �� ���� ������ ���������� �� ��������� ���������:
*	pop ���������� nullptr ��� ������� ������, ���� ���� ������;
*   push ������� ����������, ���� ������� ����������� ������� ��������� �������� ������.
*/
template<typename T> class Stack
{
	T** mContainer;
	uint64_t mContainerCapa�ity = 0;
	uint64_t mContainerSize;

public:

	Stack(uint64_t size) : mContainerSize(size){
		mContainer = new T*[size];
	}

	// ������ �� ����� ��������� �� ������ (�� ��� ��������� ������ ������� ������)
	T* popPtr() {
		if (mContainerCapa�ity == 0) {
			return nullptr;
		}
		return mContainer[mContainerCapa�ity--];
	}
	// ������ �� ����� ��������� �� ������ (�� ��� ��������� ������ ������� ������)
	T pop() {
		if (mContainerCapa�ity == 0) {
			return T();
		}
		T* ptr = mContainer[mContainerCapa�ity--];
		T copy = *(ptr);
		delete ptr;
		return copy;
	}

	// ��������� � ���� ����� ��������
	void push(T obj) {
		if (mContainerCapa�ity == mContainerSize) {
			throw std::exception("Stack is full!");
		}

		T* copy = new T;
		*copy = obj;
		mContainer[++mContainerCapa�ity] = copy;
	}

	// ��������� � ���� ������� �� ���������
	void push(T* obj) {
		if (mContainerCapa�ity == mContainerSize) {
			throw std::exception("Stack is full!");
		}

		mContainer[++mContainerCapa�ity] = obj;
	}

	std::string typeName() {
		return typeid(T).name();
	}
private:
	// ������� �������� ������� �� ������ (������� ������� �������)
	void push(T& obj) {}
};

