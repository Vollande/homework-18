﻿#include <iostream>
#include <string>
#include "Stack.h"

int main()
{
    Stack<int> sInt(20);
    Stack<float> sFloat(20);
    Stack<std::string> sString(20);
    
    sInt.push(4);
    sInt.push(8);
    sInt.push(15);
    sInt.push(new int(16));
    sInt.push(23);
    sInt.push(42);
    std::cout << "int stack contains:" << std::endl;
    std::cout << sInt.pop() << std::endl;
    std::cout << sInt.pop() << std::endl;
    std::cout << sInt.pop() << std::endl;
    std::cout << sInt.pop() << std::endl;
    std::cout << sInt.pop() << std::endl;
    int* pInt = sInt.popPtr();
    std::cout << *pInt << std::endl;
    delete pInt;
    std::cout << sInt.pop() << " <- default contstuctor for type: " << sInt.typeName() << std::endl;
    std::cout << sInt.popPtr() << " <- nullptr"  << std::endl;


    sFloat.push(8.5f);
    sFloat.push(2.5f);
    std::cout << "float stack contains:" << std::endl;
    std::cout << sFloat.pop() << std::endl;
    std::cout << sFloat.pop() << std::endl;
    std::cout << sFloat.pop() << " <- default contstuctor for type: " << sFloat.typeName() << std::endl;
    std::cout << sFloat.popPtr() << " <- nullptr" << std::endl;

    sString.push("FATHER!");
    sString.push("YOUR");
    sString.push("AM ");
    sString.push("I");
    sString.push("Steck!");
    std::cout << "string stack contains:" << std::endl;
    std::cout << sString.pop() << std::endl;
    std::cout << sString.pop() << std::endl;
    std::cout << sString.pop() << std::endl;
    std::cout << sString.pop() << std::endl;
    std::cout << sString.pop() << std::endl;
    std::cout << sString.pop() << " <- default contstuctor for type: " << sString.typeName() << std::endl;
    std::cout << sString.popPtr() << " <- nullptr" << std::endl;


    Stack<double> sDouble(1);
    sDouble.push(6.0);
    std::cout << "add to stack values more then stack size:" << std::endl;
    try {
        sDouble.push(9.0);
        std::cout << "method push need send exeption!" << std::endl;
    } catch (std::exception& e) {
        std::cout << "method push need success send exception: " << e.what() << std::endl;
    }
    try {
        sDouble.push(new double(9.0));
        std::cout << "method push need send exeption!" << std::endl;
    }
    catch (std::exception& e) {
        std::cout << "method push need success send exception: " << e.what() << std::endl;
    }
}

